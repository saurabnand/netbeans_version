package com.vodafone.api.ussd.Controller;

import com.vodafone.api.ussd.main.Service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import java.util.Hashtable;

@RestController
public class BusinessController {
    @Autowired
    BusinessService service;

    @RequestMapping(value = "/getBusinessDetails", method = RequestMethod.GET)
    @ResponseBody
    public Hashtable<String, Object> getBusinessDetails(@RequestParam(value = "business") String business) {
        return service.getBusinessDetails(business);
    }

    @RequestMapping(value = "/businesspaymentNoFee", method = RequestMethod.GET) // check
    @ResponseBody
    public Hashtable<String, Object> businessPaymentNoFee(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "businessno") String businessNo, @RequestParam(value = "account_no") String accountNo,
            @RequestParam(value = "payment_amount") float paymentAmount, @RequestParam(value = "cus_pin") int pin,
            @RequestParam(value = "payment_type") String paymentType) {
        return service.businessPaymentNoFee(access, customerNumber, businessNo, accountNo, paymentAmount, pin,
                paymentType);
    }

    @RequestMapping(value = "/validateBusinessNo", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> validateBusinessNo(@RequestParam(value = "business_no") String businessNo) {
        return service.validateBusinessNo(businessNo);
    }

    @RequestMapping(value = "/getFEAAmount", method = RequestMethod.GET)
    @ResponseBody
    public Hashtable<String, Object> getFEAAmount(@RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "pin") int pin) {
        return service.getFEAAmount(customerNumber, pin);
    }

    @RequestMapping(value = "/getWaterAmount", method = RequestMethod.GET)
    @ResponseBody
    public Hashtable<String, Object> getWaterAmount(@RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "pin") int pin) {
        return service.getWaterAmount(customerNumber, pin);
    }

    @RequestMapping(value = "/getTelecomAmount", method = RequestMethod.GET)
    @ResponseBody
    public Hashtable<String, Object> getTelecomAmount(@RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "pin") int pin) {
        return service.getTelecomAmount(customerNumber, pin);
    }

    @RequestMapping(value = "/getSkyBillAmount", method = RequestMethod.GET)
    @ResponseBody
    public Hashtable<String, Object> getSkyBillAmount(@RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "pin") int pin) {
        return service.getSkyBillAmount(customerNumber, pin);
    }

    @RequestMapping(value = "/feabillpay", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> payFeaBill(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber, @RequestParam(value = "account") String account,
            @RequestParam(value = "bill_amount") float billAmount, @RequestParam(value = "cus_pin") int pin) {
        return service.feaBillPay(access, customerNumber, account, billAmount, pin);
    }

    @RequestMapping(value = "/waterbillpay", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> payWaterBill(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber, @RequestParam(value = "meter") String meter,
            @RequestParam(value = "name") String name, @RequestParam(value = "money") float money,
            @RequestParam(value = "cus_pin") int pin) {
        return service.waterBillPay(access, customerNumber, meter, name, money, pin);
    }

    @RequestMapping(value = "/businesspayment", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> businessPayment(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "businessno") String businessNo, @RequestParam(value = "account_no") String accountNo,
            @RequestParam(value = "payment_amount") float paymentAmount, @RequestParam(value = "cus_pin") int pin,
            @RequestParam(value = "payment_type") String paymentType) {
        return service.businessPayment(access, customerNumber, businessNo, accountNo, paymentAmount, pin, paymentType);
    }

    @RequestMapping(value = "/telecompay", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> payTelecom(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber, @RequestParam(value = "account") String account,
            @RequestParam(value = "bill_amount") float billAmount, @RequestParam(value = "cus_pin") int pin) {
        return service.telecomPay(access, customerNumber, account, billAmount, pin);
    }

    @RequestMapping(value = "/skypay", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> paySky(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber, @RequestParam(value = "account") String account,
            @RequestParam(value = "bill_amount") float billAmount, @RequestParam(value = "cus_pin") int pin) {
        return service.skyPay(access, customerNumber, account, billAmount, pin);
    }

    @RequestMapping(value = "/businesspaymentpromo", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> businessPaymentPromo(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "businessno") String businessNo,
            @RequestParam(value = "payment_amount") float paymentAmount, @RequestParam(value = "cus_pin") int pin) {
        return service.businessPaymentPromo(access, customerNumber, businessNo, paymentAmount, pin);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Hashtable<String, Object> handleMissingParams(MissingServletRequestParameterException ex) {
        String parameterName = ex.getParameterName();
        Hashtable<String, Object> output = new Hashtable<>();
        output.put("Respose Code: ", 0);
        output.put("Error", "Missing parameter:" + " " + parameterName);
        output.put("HttpStatus Code: ", HttpStatus.BAD_REQUEST);
        return output;
    }

}