package com.vodafone.api.ussd.Controller;

import com.vodafone.api.ussd.main.Service.MainService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Hashtable;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@RestController
@EnableAutoConfiguration
public class MainController {

    @Autowired
    MainService mainService;
    
    @RequestMapping(value = "/validateNumber", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> validateCustomerNumber(@RequestParam(value = "cus_msisdn") String number) {
        return mainService.validateCustomerNumber(number);
    }
    
    @RequestMapping(value = "/validatePIN", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> validateCustomerPIN(@RequestParam(value = "msisdn") String number,
            @RequestParam(value = "pin") Integer pin) {
        return mainService.validateCustomerPin(number, pin);
    }

    @RequestMapping(value = "/validateTransferMobile", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> validateTransferMobile(@RequestParam(value = "transfer_misdn") String number) {
        return mainService.validateTransferMobile(number);
    }

    @RequestMapping(value = "/recharge/getBalance", method = RequestMethod.GET)
    @ResponseBody
    public Hashtable<String, Object> getRecharge(@RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "pin") int pin) {
        return mainService.getAccountBalance(customerNumber, pin);
    }

    @RequestMapping(value = "/getPaymentAmount", method = RequestMethod.GET)
    @ResponseBody
    public Hashtable<String, Object> getPaymentAmount(@RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "pin") int pin) {
        return mainService.getPaymentAmount(customerNumber, pin);
    }

    @RequestMapping(value = "/changepin", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> changePin(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber, @RequestParam(value = "cus_oldpin") int oldPin,
            @RequestParam(value = "cus_newpin") int newPin,
            @RequestParam(value = "cus_confirmnewpin") int confirmNewPin) {
        return mainService.changePin(access, customerNumber, oldPin, newPin, confirmNewPin);
    }

    @RequestMapping(value = "/ministatement", method = RequestMethod.GET)
    @ResponseBody
    public Hashtable<String, Object> miniStatement(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber, @RequestParam(value = "cus_pin") int pin) {
        return mainService.miniStatement(access, customerNumber, pin);
    }
    
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Hashtable<String, Object> handleMissingParams(MissingServletRequestParameterException ex) {
        String parameterName = ex.getParameterName();
        Hashtable<String, Object> output = new Hashtable<>();
        output.put("Respose Code: ", 0);
        output.put("Error", "Missing parameter:" + " " + parameterName);
        output.put("HttpStatus Code: ", HttpStatus.BAD_REQUEST);
        return output;
    }
    
}
