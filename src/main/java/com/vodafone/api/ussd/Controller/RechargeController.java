package com.vodafone.api.ussd.Controller;

import com.vodafone.api.ussd.main.Service.RechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.http.HttpStatus;
import java.util.Hashtable;

@RestController
public class RechargeController {
    @Autowired
    RechargeService rechargeService;

    @RequestMapping(value = "/validateRechargeAmount", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> validateRechargeAmount(@RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "amount") float amount) {
        return rechargeService.validateRechargeAmount(customerNumber, amount);
    }

    @RequestMapping(value = "/rechargePersonalAccount", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> rechargeAmount(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "recharge_amount") float recharge_amount, @RequestParam(value = "cus_pin") int pin) {
        return rechargeService.rechargePersonalAccount(access, customerNumber, recharge_amount, pin);
    }

    @RequestMapping(value = "/rechargeAnotherAccount", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> rechargeAnother(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber, @RequestParam(value = "b_msisdn") String bNumber,
            @RequestParam(value = "recharge_amount") float rechargeAmount, @RequestParam(value = "cus_pin") int pin) {
        return rechargeService.rechargeAnotherAccount(access, customerNumber, bNumber, rechargeAmount, pin);
    }
    
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Hashtable<String, Object> handleMissingParams(MissingServletRequestParameterException ex) {
        String parameterName = ex.getParameterName();
        Hashtable<String, Object> output = new Hashtable<>();
        output.put("Respose Code: ", 0);
        output.put("Error", "Missing parameter:" + " " + parameterName);
        output.put("HttpStatus Code: ", HttpStatus.BAD_REQUEST);
        return output;
    }

}
