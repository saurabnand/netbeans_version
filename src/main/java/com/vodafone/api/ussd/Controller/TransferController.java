package com.vodafone.api.ussd.Controller;

import com.vodafone.api.ussd.main.Service.TransferService;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.http.HttpStatus;
import java.util.Hashtable;

@RestController
public class TransferController {

    @Autowired
    TransferService transferService;

    @RequestMapping(value = "/getTransferAmount", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> getTransferAmount(@RequestParam(value = "cus_msisdn") String number,
            @RequestParam(value = "transfer_msisdn") String transferNumber,
            @RequestParam(value = "amount") float amount) {
        return transferService.getTransferAmount(number, transferNumber, amount);
    }

    @RequestMapping(value = "/transferAmount", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> transfer(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String number, @RequestParam(value = "b_msisdn") String bNumber,
            @RequestParam(value = "transfer_amount") Float amount, @RequestParam(value = "cus_pin") int pin) {
        return transferService.transfer(access, number, bNumber, amount, pin);
    }

    @RequestMapping(value = "/aft_getTransferAmount", method = RequestMethod.GET)
    @ResponseBody
    public Hashtable<String, Object> aft_getTransfer(@RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "cardnumber") String cardNumber) {
        return transferService.aft_getTransferAmount(customerNumber, cardNumber);
    }

    @RequestMapping(value = "/mpaisatocard", method = RequestMethod.POST)
    @ResponseBody
    public Hashtable<String, Object> transferToCard(@RequestParam(value = "access") String access,
            @RequestParam(value = "cus_msisdn") String customerNumber,
            @RequestParam(value = "cardnumber") String cardNumber,
            @RequestParam(value = "transfer_amount") float transferAmount, @RequestParam(value = "cus_pin") int pin) {
        return transferService.mpaisaToCardTransfer(access, customerNumber, cardNumber, transferAmount, pin);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Hashtable<String, Object> handleMissingParams(MissingServletRequestParameterException ex) {
        String parameterName = ex.getParameterName();
        Hashtable<String, Object> output = new Hashtable<>();
        output.put("Respose Code: ", 0);
        output.put("Error", "Missing parameter:" + " " + parameterName);
        output.put("HttpStatus Code: ", HttpStatus.BAD_REQUEST);
        return output;
    }
}
