package com.vodafone.api.ussd.main.Model;

import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

import javax.persistence.Entity;
import javax.persistence.Id;

@NamedStoredProcedureQueries({
                @NamedStoredProcedureQuery(name = "MPS_ValidateCustomerMobile", procedureName = "MPS_ValidateCustomerMobile", parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
                }),
                @NamedStoredProcedureQuery(name = "MPS_ValidateCustomerPIN", procedureName = "MPS_ValidateCustomerPIN", parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "msisdn", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "pin", type = Integer.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
                }),
                @NamedStoredProcedureQuery(name = "MPS_ValidateTransferMobile", procedureName = "MPS_ValidateTransferMobile", parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "transfer_msisdn", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
                }),
                @NamedStoredProcedureQuery(name = "MPS_GetAccountBalance", procedureName = "MPS_GetAccountBalance", parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "pin", type = Integer.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "amount", type = Float.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
                }),
                @NamedStoredProcedureQuery(name = "MPS_GetPaymentAmount", procedureName = "MPS_GetPaymentAmount", parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "pin", type = Integer.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "amount", type = Float.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
                }),
                @NamedStoredProcedureQuery(name = "MPS_ChangePIN", procedureName = "MPS_ChangePIN", parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_oldpin", type = Integer.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_newpin", type = Integer.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_confirmnewpin", type = Integer.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
                }),
                @NamedStoredProcedureQuery(name = "MPS_MiniStatement", procedureName = "MPS_MiniStatement", parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
                })
})

@Entity
public class Main {
        @Id
        private long id;
        private String results;

        public Main(long id, String results) {
                this.id = id;
                this.results = results;
        }

        public Main() {
        }

        public long getId() {
                return id;
        }

        public String getResults() {
                return results;
        }

}
