package com.vodafone.api.ussd.main.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Hashtable;

import javax.persistence.EntityManager;

@Service
@Component
public class RechargeService {
    @Autowired
    private EntityManager entityManager;
    private Utils utils = new Utils();

    public Hashtable<String, Object> validateRechargeAmount(String customerNumber, float amount) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("cus_msisdn", customerNumber);
        params.put("amount", amount);
        return utils.callStoredProcedureDefaultOutput("MPS_ValidateRechargeAmount", params, entityManager);

    }

    public Hashtable<String, Object> rechargePersonalAccount(String access, String customerNumber, float recharge_amount,
            int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("recharge_amount", recharge_amount);
        params.put("cus_pin", pin);
        return utils.callStoredProcedureDefaultOutput("MPS_Recharge_TestNew", params, entityManager);

    }

    public Hashtable<String, Object> rechargeAnotherAccount(String access, String customerNumber, String bNumber,
            float recharge_amount, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("b_msisdn", bNumber);
        params.put("recharge_amount", recharge_amount);
        params.put("cus_pin", pin);
        return utils.callStoredProcedureDefaultOutput("MPS_AnotherRecharge_testNew", params, entityManager);
    }

}
