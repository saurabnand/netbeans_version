package com.vodafone.api.ussd.main.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.ParameterMode;

@NamedStoredProcedureQueries({

        @NamedStoredProcedureQuery(name = "MPS_ValidateRechargeAmount", procedureName = "MPS_ValidateRechargeAmount", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_Recharge_TestNew", procedureName = "MPS_Recharge_TestNew", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "recharge_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "receipt", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_AnotherRecharge_testNew", procedureName = "MPS_AnotherRecharge_testNew", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "b_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "recharge_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "receipt", type = Integer.class)
        })
})

@Entity
public class Recharge {
    @Id
    private long id;
    private String response;

    public Recharge(long id, String response) {
        this.id = id;
        this.response = response;
    }

    public Recharge() {
    }

    public long getId() {
        return this.id;
    }

    public String getResponse() {
        return response;
    }

}
