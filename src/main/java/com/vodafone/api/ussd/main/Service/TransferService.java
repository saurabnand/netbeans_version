package com.vodafone.api.ussd.main.Service;

import java.util.Hashtable;

import javax.persistence.EntityManager;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
@Component
public class TransferService {
    @Autowired
    private EntityManager entityManager;
    private Utils utils = new Utils();

    public Hashtable<String, Object> getTransferAmount(String phoneNumber, String transferNumber, Float amount) {
        Hashtable<String, Object> paramsHash = new Hashtable<String, Object>();
        paramsHash.put("cus_msisdn", phoneNumber);
        paramsHash.put("transfer_msisdn", transferNumber);
        paramsHash.put("amount", amount);

        String[] outparameters = { "amount", "response" };
        return utils.callStoredProcedureOutPut("MPS_GetTransferAmount", paramsHash, this.entityManager, outparameters);

    }

    public Hashtable<String, Object> transfer(String access, String customerNumber, String bPhoneNumber, Float amount,
            int pin) {
        Hashtable<String, Object> paramsHash = new Hashtable<String, Object>();
        paramsHash.put("access", access);
        paramsHash.put("cus_msisdn", customerNumber);
        paramsHash.put("b_msisdn", bPhoneNumber);
        paramsHash.put("transfer_amount", amount);
        paramsHash.put("cus_pin", pin);

        return utils.callStoredProcedureDefaultOutput("MPS_Transfer", paramsHash, this.entityManager);

    }

    // TO-DO Return 2 objects = JSON object should be used here
    public Hashtable<String, Object> aft_getTransferAmount(String customerNumber, String cardNumber) {
        Hashtable<String, Object> paramsHash = new Hashtable<>();
        paramsHash.put("cus_msisdn", customerNumber);
        paramsHash.put("cardnumber", cardNumber);

        String[] outparameters = { "amount", "response" };
        return utils.callStoredProcedureOutPut("MPS_AFC_GetTransferAmount", paramsHash, entityManager, outparameters);
    }

    public Hashtable<String, Object> mpaisaToCardTransfer(String access, String customerNumber, String cardNumber,
            float transferAmount, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("cardnumber", cardNumber);
        params.put("transfer_amount", transferAmount);
        params.put("cus_pin", pin);

        return utils.callStoredProcedureDefaultOutput("MPS_MpaisaToCardTransfer", params, entityManager);
    }

}
