package com.vodafone.api.ussd.main.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Hashtable;

import javax.persistence.EntityManager;

@Service
@Component
public class BusinessService {
    @Autowired
    private EntityManager entityManager;
    private Utils utils = new Utils();

    public Hashtable<String, Object> getBusinessDetails(String business) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("business", business);
        String[] outputParameters = { "business_one", "business_no_one", "business_two", "business_no_two",
                "business_three", "business_no_three", "business_four", "business_no_four", "business_five",
                "business_no_five" };
        return utils.callStoredProcedureOutPut("MPS_GetBusinessDetails", params, entityManager, outputParameters);
    }

    public Hashtable<String, Object> businessPaymentNoFee(String access, String customerNumber, String businessNo,
            String accountNo, float paymentAmount, int pin, String paymentType) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("businessno", businessNo);
        params.put("account_no", accountNo);
        params.put("payment_amount", paymentAmount);
        params.put("cus_pin", pin);
        params.put("payment_type", paymentType);

        return utils.callStoredProcedureDefaultOutput("MPS_BusinessPayment_NoFee", params, entityManager);
    }

    public Hashtable<String, Object> getBusinessDisplayText(String access, int businessid, String type) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("businessid", businessid);
        params.put("type", type);

        String[] outParameters = { "displaytext", "response" };
        return utils.callStoredProcedureOutPut("MPS_GetBusinessDisplayText", params, entityManager, outParameters);
    }

    public Hashtable<String, Object> validateBusinessNo(String businessNo) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("business_no", businessNo);

        String[] outParameters = { "business_name", "business_category", "account_name", "account_type1",
                "account_type2", "account_type3", "account_type4", "account_type5", "account_type6", "account_type7",
                "account_type8", "account_type9", "response" };
        return utils.callStoredProcedureOutPut("MPS_ValidateBusinessNo", params, entityManager, outParameters);
    }

    public Hashtable<String, Object> getFEAAmount(String customerNumber, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("cus_msisdn", customerNumber);
        params.put("pin", pin);

        String[] outParameters = { "amount", "response" };
        return utils.callStoredProcedureOutPut("MPS_GetFEAAmount", params, entityManager, outParameters);
    }

    public Hashtable<String, Object> getWaterAmount(String customerNumber, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("cus_msisdn", customerNumber);
        params.put("pin", pin);

        String[] outParameters = { "amount", "response" };
        return utils.callStoredProcedureOutPut("MPS_GetWaterAmount", params, entityManager, outParameters);
    }

    public Hashtable<String, Object> getTelecomAmount(String customerNumber, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("cus_msisdn", customerNumber);
        params.put("pin", pin);

        String[] outParameters = { "amount", "response" };
        return utils.callStoredProcedureOutPut("MPS_GetTelecomAmount", params, entityManager, outParameters);
    }

    public Hashtable<String, Object> getSkyBillAmount(String customerNumber, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("cus_msisdn", customerNumber);
        params.put("pin", pin);

        String[] outParameters = { "amount", "response" };
        return utils.callStoredProcedureOutPut("MPS_GetSkyBillAmount", params, entityManager, outParameters);
    }

    public Hashtable<String, Object> feaBillPay(String access, String customerNumber, String account, float billAmount,
            int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("account", account);
        params.put("bill_amount", billAmount);
        params.put("cus_pin", pin);

        return utils.callStoredProcedureDefaultOutput("MPS_FEABillPay", params, entityManager);
    }

    public Hashtable<String, Object> waterBillPay(String access, String customerNumber, String meter, String name,
            float money, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("meter", meter);
        params.put("name", name);
        params.put("money", money);
        params.put("cus_pin", pin);

        return utils.callStoredProcedureDefaultOutput("MPS_WaterBillPay", params, entityManager);
    }

    public Hashtable<String, Object> businessPayment(String access, String customerNumber, String businessNo,
            String accountNo, float paymentAmount, int pin, String paymentType) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("buinessno", businessNo);
        params.put("account_no", accountNo);
        params.put("payment_amount", paymentAmount);
        params.put("cus_pin", pin);
        params.put("payment_type", paymentType);

        return utils.callStoredProcedureDefaultOutput("MPS_BusinessPayment", params, entityManager);
    }

    public Hashtable<String, Object> telecomPay(String access, String customerNumber, String account, float billAmount,
            int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("account", account);
        params.put("bill_amount", billAmount);
        params.put("cus_pin", pin);

        return utils.callStoredProcedureDefaultOutput("MPS_TelecomBillPay", params, entityManager);
    }

    public Hashtable<String, Object> skyPay(String access, String customerNumber, String account, float billAmount,
            int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("account", account);
        params.put("bill_amount", billAmount);
        params.put("cus_pin", pin);

        return utils.callStoredProcedureDefaultOutput("MPS_SKYBillPay", params, entityManager);
    }

    public Hashtable<String, Object> businessPaymentPromo(String access, String customerNumber, String businessNo,
            float paymentAmount, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("businessno", businessNo);
        params.put("payment_amount", paymentAmount);
        params.put("cus_pin", pin);

        return utils.callStoredProcedureDefaultOutput("MPS_BusinessPayment_Promo", params, entityManager);
    }
}
