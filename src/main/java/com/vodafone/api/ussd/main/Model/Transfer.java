package com.vodafone.api.ussd.main.Model;

import java.awt.geom.Arc2D.Float;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "MPS_GetTransferAmount", procedureName = "MPS_GetTransferAmount", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "transfer_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_Transfer", procedureName = "MPS_Transfer", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "b_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "transfer_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_AFC_GetTransferAmount", procedureName = "MPS_AFC_GetTransferAmount", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cardnumber", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_MpaisaToCardTransfer", procedureName = "MPS_MpaisaToCardTransfer", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cardnumber", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "transfer_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        })

})

@Entity
public class Transfer {
    @Id
    private long id;
    private String results;

    public Transfer(long id, String results) {
        this.id = id;
        this.results = results;
    }

    public Transfer() {
    }

    public long getId() {
        return id;
    }

    public String getResults() {
        return results;
    }
}
