package com.vodafone.api.ussd.main.Service;

import java.util.Hashtable;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;




public class Utils {

    
    public Hashtable<String,Object> callStoredProcedureDefaultOutput(String queryName, Hashtable<String, Object> storeParams, EntityManager entityManager){
        StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery(queryName);
        for(Map.Entry<String,Object> params:storeParams.entrySet()){
            query.setParameter(params.getKey(), params.getValue());
        }

        Hashtable<String,Object> output = new Hashtable<>();

        try{
            
            query.execute();
 
            int response =Integer.parseInt(query.getOutputParameterValue("response").toString());
            output.put("response", response);
        }catch(Exception e){
            output.put("response", 0);
        }
        
        return output;
    }

    public Hashtable<String,Object> callStoredProcedureOutPut (String queryName, Hashtable<String, Object> storeParams, EntityManager entityManager,String[] outParameters){
        StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery(queryName);
        for(Map.Entry<String,Object> params:storeParams.entrySet()){
            query.setParameter(params.getKey(), params.getValue());
        }
        query.execute();
        Hashtable<String,Object> output = new Hashtable<>();
        if(outParameters.length>0){
            try{
                    
                for(int i=0; i<outParameters.length;i++){

                    output.put(outParameters[i], query.getOutputParameterValue(outParameters[i]));
                }
            }catch(Exception e){
                output.put("response", 0);
            }
        }

        return output;

    }
    
    
}
