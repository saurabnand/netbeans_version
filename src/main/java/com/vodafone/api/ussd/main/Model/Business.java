package com.vodafone.api.ussd.main.Model;

import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

import javax.persistence.Entity;
import javax.persistence.Id;

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "MPS_GetBusinessDetails", procedureName = "MPS_GetBusinessDetails", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "business", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_one", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_no_one", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_two", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_no_two", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_three", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_no_three", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_four", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_no_four", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_five", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_no_five", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_BusinessPayment_NoFee", procedureName = "MPS_BusinessPayment_NoFee", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "businessno", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "account_no", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "payment_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "payment_type", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_GetBusinessDisplayText", procedureName = "MPS_GetBusinessDisplayText", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "businessid", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "type", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "displaytext", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_ValidateBusinessNo", procedureName = "MPS_ValidateBusinessNo", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "business_no", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_name", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "business_category", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_name", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_type1", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_type2", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_type3", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_type4", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_type5", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_type6", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_type7", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_type8", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "account_type9", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_GetFEAAmount", procedureName = "MPS_GetFEAAmount", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_GetWaterAmount", procedureName = "MPS_GetWaterAmount", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_GetTelecomAmount", procedureName = "MPS_GetTelecomAmount", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_GetSkyBillAmount", procedureName = "MPS_GetSkyBillAmount", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_FEABillPay", procedureName = "MPS_FEABillPay", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "account", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "bill_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)

        }),
        @NamedStoredProcedureQuery(name = "MPS_WaterBillPay", procedureName = "MPS_WaterBillPay", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "meter", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "name", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "money", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_BusinessPayment", procedureName = "MPS_BusinessPayment", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "businessno", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "account_no", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "payment_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "payment_type", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_TelecomBillPay", procedureName = "MPS_TelecomBillPay", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "account", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "bill_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)

        }),
        @NamedStoredProcedureQuery(name = "MPS_SKYBillPay", procedureName = "MPS_SKYBillPay", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "account", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "bill_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = String.class)
        }),
        @NamedStoredProcedureQuery(name = "MPS_BusinessPayment_Promo", procedureName = "MPS_BusinessPayment_Promo", parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "access", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_msisdn", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "businessno", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "payment_amount", type = Float.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "cus_pin", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "response", type = Integer.class)
        })
})

@Entity
public class Business {
    @Id
    private long id;
    private String response;

    public Business(long id, String response) {
        this.id = id;
        this.response = response;

    }

    public Business() {
    }

    public long getId() {
        return id;
    }

    public String getResponse() {
        return response;
    }

}
