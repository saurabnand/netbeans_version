package com.vodafone.api.ussd.main.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import java.util.Hashtable;

@Service
@Component
public class MainService {
    @Autowired
    private EntityManager entityManager;
    private Utils utils = new Utils();

    public Hashtable<String, Object> validateCustomerNumber(String phoneNumber) {
        Hashtable<String, Object> paramsHash = new Hashtable<String, Object>();
        paramsHash.put("cus_msisdn", phoneNumber);
        
        return utils.callStoredProcedureDefaultOutput("MPS_ValidateCustomerMobile", paramsHash, this.entityManager);
    }

    public Hashtable<String, Object> validateCustomerPin(String phoneNumber, Integer pinNumber) {
        Hashtable<String, Object> paramsHash = new Hashtable<String, Object>();
        paramsHash.put("msisdn", phoneNumber);
        paramsHash.put("pin", pinNumber);
        return utils.callStoredProcedureDefaultOutput("MPS_ValidateCustomerPIN", paramsHash, this.entityManager);
    }

    public Hashtable<String, Object> validateTransferMobile(String phoneNumber) {
        Hashtable<String, Object> paramsHash = new Hashtable<String, Object>();
        paramsHash.put("transfer_msisdn", phoneNumber);
        return utils.callStoredProcedureDefaultOutput("MPS_ValidateTransferMobile", paramsHash, this.entityManager);

    }

    public Hashtable<String, Object> getAccountBalance(String customerNumber, int pin) {
        Hashtable<String, Object> paramsHash = new Hashtable<String, Object>();
        paramsHash.put("cus_msisdn", customerNumber);
        paramsHash.put("pin", pin);

        String[] outputParams = { "amount", "response" };

        return utils.callStoredProcedureOutPut("MPS_GetAccountBalance", paramsHash, entityManager, outputParams);

    }

    public Hashtable<String, Object> getPaymentAmount(String customerNumber, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("cus_msisdn", customerNumber);
        params.put("pin", pin);

        String[] outputParams = { "amount", "response" };
        return utils.callStoredProcedureOutPut("MPS_GetPaymentAmount", params, entityManager, outputParams);
    }

    public Hashtable<String, Object> changePin(String access, String customerNumber, int oldPin, int newPin,
            int confirmNewPin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("cus_oldpin", oldPin);
        params.put("cus_newpin", newPin);
        params.put("cus_confirmnewpin", confirmNewPin);

        return utils.callStoredProcedureDefaultOutput("MPS_ChangePIN", params, entityManager);
    }

    public Hashtable<String, Object> miniStatement(String access, String customerNumber, int pin) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("access", access);
        params.put("cus_msisdn", customerNumber);
        params.put("cus_pin", pin);

        return utils.callStoredProcedureDefaultOutput("MPS_MiniStatement", params, entityManager);

    }

}
